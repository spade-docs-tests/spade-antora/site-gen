all: ui site
.PHONY: site ui

site:
	npx antora --fetch antora-playbook.yml

ui:
	(cd ui \
		&& gulp bundle)

open:
	xdg-open build/site/index.html
