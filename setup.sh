#!/usr/bin/env sh
git clone https://gitlab.com/spade-docs-tests/spade-antora/docs.git docs
git clone https://gitlab.com/spade-docs-tests/spade-antora/ui.git ui
pushd ui
npm install
popd
